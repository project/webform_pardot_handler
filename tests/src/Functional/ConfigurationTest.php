<?php

namespace Drupal\Tests\webform_pardot_handler\Functional;

use Drupal\webform\Entity\Webform;
use Drupal\Tests\webform\Functional\WebformBrowserTestBase;
use Drupal\Core\Config\FileStorage;

/**
 * Tests configuring the form handler.
 *
 * @group webform_pardot_handler
 */
class ConfigurationTest extends WebformBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'webform',
    'webform_pardot_handler',
  ];

  /**
   * Webforms to load.
   *
   * @var array
   */
  protected static $testWebforms = ['multi_step_pardot_test_form'];

  /**
   * Lazy load a test webform.
   *
   * @param string $id
   *   Webform id.
   *
   * @return \Drupal\webform\WebformInterface|null
   *   A webform.
   *
   * @see \Drupal\views\Tests\ViewTestData::createTestViews
   */
  protected function loadWebform($id) {
    $storage = \Drupal::entityTypeManager()->getStorage('webform');
    if ($webform = $storage->load($id)) {
      return $webform;
    }
    else {
      $config_name = 'webform.webform.' . $id;
      $config_directory = drupal_get_path('module', 'webform_pardot_handler') . '/tests/modules/test/config/install';
      if (!file_exists("$config_directory/$config_name.yml")) {
        throw new \Exception("Webform $id does not exist in $config_directory");
      }

      $file_storage = new FileStorage($config_directory);
      $values = $file_storage->read($config_name);
      $webform = $storage->create($values);
      $webform->save();
      return $webform;
    }
  }

  /**
   * Tests a multi-step form.
   */
  public function testEditHandler() {
    $this->drupalLogin($this->rootUser);

    $webformId = 'multi_step_pardot_test_form';
    $handlerId = 'submit_data_to_pardot';
    $handlerEditPath = "/admin/structure/webform/manage/{$webformId}/handlers/{$handlerId}/edit";

    // Invalid URLs should not be allowed.
    $this->drupalPostForm($handlerEditPath, [
      'settings[pardot_url]' => 'foo',
    ], 'Save');
    $this->assertText('Pardot post url must be a valid URL.');

    // Valid URLs with extra whitespace should be accepted, but the whitespace
    // should be trimmed.
    $this->drupalPostForm($handlerEditPath, [
      'settings[pardot_url]' => ' https://example.com/ ',
    ], 'Save');
    $this->assertText('The webform handler was successfully updated.');

    /** @var \Drupal\webform\Entity\Webform $webform */
    $webform = Webform::load($webformId);
    $handler = $webform->getHandler($handlerId);
    $configuration = $handler->getConfiguration();
    $settings = !empty($configuration['settings']) ? $configuration['settings'] : [];
    self::assertTrue(isset($settings['pardot_url']) && $settings['pardot_url'] === 'https://example.com/', 'Whitespace is trimmed from the Pardot URL.');

    $this->drupalPostForm($handlerEditPath, [
      'settings[pardot_url]' => 'https://example.com/pardot',
    ], 'Save');
    $this->assertText('The webform handler was successfully updated.');
  }

}
