<?php

namespace Drupal\Tests\webform_pardot_handler\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\webform\Entity\Webform;

/**
 * Tests for multi-step webforms.
 *
 * @group webform_pardot_handler
 */
class MultistepFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';


  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'node',
    'webform',
    'webform_pardot_handler_test',
  ];

  /**
   * Tests a multi-step form.
   */
  public function testMultistepForm() {
    $webform_submissions = \Drupal::entityTypeManager()
      ->getStorage('pardot_submission')
      ->loadMultiple();
    $numInitialSubmissions = count($webform_submissions);

    $webform = Webform::load('multi_step_pardot_test_form');
    $this->drupalPostForm($webform->toUrl(), [
      'first_page_textbox' => 'anything',
    ], 'Next >');
    $this->submitForm([
      'second_page_textbox' => 'anything',
    ], 'Submit');

    $webform_submissions = \Drupal::entityTypeManager()
      ->getStorage('pardot_submission')
      ->loadMultiple();
    self::assertEquals(1, count($webform_submissions) - $numInitialSubmissions, 'One pardot submission is created for multi-step forms.');
  }

}
