<?php

namespace Drupal\Tests\webform_pardot_handler\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for pardot submission list builder.
 *
 * @group webform_pardot_handler
 */
class PardotSubmissionListBuilderTest extends BrowserTestBase {
  
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'node',
    'webform',
    'webform_pardot_handler',
    'webform_test_submissions',
  ];

  /**
   * Tests the webform overview page.
   */
  public function testWebformOverview() {
    $assert_session = $this->assertSession();

    // Test with a superuser.
    $any_webform_user = $this->createUser([
      'access webform overview',
      'create webform',
      'edit any webform',
      'view pardot_submission',
    ]);

    // webform_test_submissions would create test submissions.
    $webform_submission = \Drupal::entityTypeManager()
      ->getStorage('webform_submission')
      ->load('1');

    // Create a pardot submission to see if it gets listed in entity list view.
    \Drupal::entityTypeManager()
      ->getStorage('pardot_submission')
      ->create([
        'webform_submission' => $webform_submission,
        'status' => 'queued',
      ])->save();

    $this->drupalLogin($any_webform_user);
    $list_path = '/admin/structure/webform/pardot_submissions';
    $this->drupalGet($list_path);

    // Ensure that the enity list builder have required columns.
    $assert_session->pageTextContains('Pardot Submission ID');
    $assert_session->pageTextContains('Webforms Submission');
    $assert_session->pageTextContains('Status');
    $assert_session->pageTextContains('Log');
    $assert_session->pageTextContains('Operations');
    $assert_session->pageTextContains('queued');
    $assert_session->linkExists('Test: Submissions: Submission #1');

  }

}
