<?php

namespace Drupal\Tests\webform_pardot_handler\Unit;

use Drupal\Tests\UnitTestCase;

use Drupal\webform_pardot_handler\Entity\PardotSubmissionInterface;
use Drupal\webform_pardot_handler\PardotHandler;

use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Entity\Webform;
use Drupal\webform\Plugin\WebformHandlerBase;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7\FnStream;

/**
 * @coversDefaultClass \Drupal\webform_pardot_handler\PardotHandler
 *
 * @group webform_pardot_handler
 */
class PardotHandlerTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->pardot_handler = new PardotHandler(
      $this->mockHttpClient(),
      $this->mockEntityTypeManager()
    );

    // Mock pardot handler by setting the error response.
    $this->pardot_error_handler = new PardotHandler(
      $this->mockHttpClient(TRUE),
      $this->mockEntityTypeManager(TRUE)
    );
  }

  /**
   * Tests the submitDataToPardot function.
   *
   * ::covers submitDataToPardot.
   */
  public function testSubmitDataToPardot() {
    // Tests the submitDataToPardot() method and ensure that the
    // method gets executed without error and we get the expected response.
    $output = $this->pardot_handler->submitDataToPardot('1');

    // We should get status code as processed if no error occured.
    $this->assertEquals('processed', $output->getStatus());

    // Mocks the pardot handler with error response to see if we get error
    // status.
    $output = $this->pardot_error_handler->submitDataToPardot('1');
    $this->assertEquals('error', $output->getStatus());
  }

  /**
   * Tests mapping webform submission data to pardot data.
   */
  public function testDataMapping() {
    // Reflection makes this private method accessible to be unit tested since
    // that's the most accessible way to assert that it transforms submission
    // data as intended.
    $class = new \ReflectionClass(PardotHandler::class);
    $method = $class->getMethod('mapData');
    $method->setAccessible(TRUE);

    $pardotHandler = new PardotHandler($this->mockHttpClient(), $this->mockEntityTypeManager());

    $data = $method->invoke(
      $pardotHandler,
      'foo.foo2.foo3|pardot3',
      [
        'foo' => [
          'foo2' => [
            'foo3' => 'foo3value',
          ],
        ],
        'bar' => 'barvalue',
      ]
    );

    self::assertEquals('foo3value', $data['pardot3']);
    self::assertEquals('barvalue', $data['bar']);

    $data = $method->invoke(
      $pardotHandler,
      'foo[foo2][foo3|pardot3',
      [
        'foo' => [
          'foo2' => [
            'foo3' => 'foo3value',
          ],
        ],
        'bar' => 'barvalue',
      ]
    );

    self::assertEquals('foo3value', $data['pardot3']);
    self::assertEquals('barvalue', $data['bar']);
  }

  /**
   * Mocks entity type manager.
   */
  protected function mockEntityTypeManager($error = FALSE) {
    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);

    $entity_type_manager->getStorage('pardot_submission')
      ->willReturn($this->mockPardotSubmissionEntityStorage($error));

    return $entity_type_manager->reveal();
  }

  /**
   * Mocks success Http Client.
   */
  protected function mockHttpClient($error = FALSE) {
    $http_client = $this->prophesize(Client::class);

    $response = $this->mockResponse($error);

    $http_client->post(
      'www.example.com', [
        'verify' => TRUE,
        'form_params' => [
          'first_name' => 'First Name',
          'last_name' => 'Last Name',
          'email' => 'mail@example.com',
        ],
        'headers' => [
          'Content-type' => 'application/x-www-form-urlencoded',
        ],
      ]
    )->willReturn($response);

    return $http_client->reveal();
  }

  /**
   * Mocks Pardot submission entity storage.
   */
  protected function mockPardotSubmissionEntityStorage($error = FALSE) {
    $entity_storage = $this->prophesize(EntityStorageInterface::class);

    $entity_storage->load('1')->willReturn($this->mockPardotSubmission($error));

    return $entity_storage->reveal();
  }

  /**
   * Mocks Pardot submission entity.
   */
  protected function mockPardotSubmission($error) {
    $pardot_submission = $this->prophesize(PardotSubmissionInterface::class);

    $pardot_submission->getWebformSubmission()
      ->willReturn($this->mockWebformSubmission());

    $pardot_submission->addLog("200: Succesfull")
      ->willReturn($this->mockPardotSubmissionEntity());
    $pardot_submission->addLog("503: Internal Server Error at Pardot")
      ->willReturn($this->mockPardotSubmissionEntity());

    $pardot_submission->setStatus("processed")
      ->willReturn($this->mockPardotSubmissionEntity());
    $pardot_submission->setStatus("error")
      ->willReturn($this->mockPardotSubmissionEntity());

    $pardot_submission->save()
      ->willReturn($this->mockPardotSubmissionEntity());

    $pardot_submission->id()->willReturn('1');

    if ($error) {
      $pardot_submission->getStatus()->willReturn('error');
    }
    else {
      $pardot_submission->getStatus()->willReturn('processed');
    }

    return $pardot_submission->reveal();
  }

  /**
   * Mocks pardot submission.
   */
  protected function mockPardotSubmissionEntity() {
    $pardot_submission = $this->prophesize(PardotSubmissionInterface::class);

    return $pardot_submission->reveal();
  }

  /**
   * Mocks webform submission entity.
   */
  public function mockWebformSubmission() {
    $webform_submission = $this->prophesize(WebformSubmission::class);

    $webform_submission->getWebform()->willReturn($this->mockWebform());
    $webform_submission->getData()->willReturn(
      [
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'mail@example.com',
      ]
    );

    return $webform_submission->reveal();
  }

  /**
   * Mocks a webform.
   */
  public function mockWebform() {
    $webform = $this->prophesize(Webform::class);

    $webform->getHandler('submit_data_to_pardot')
      ->willReturn($this->mockWebformHandler());

    return $webform->reveal();
  }

  /**
   * Mocks a webform handler.
   */
  public function mockWebformHandler() {
    $webform_handler = $this->prophesize(WebformHandlerBase::class);

    $config = [
      'settings' => [
        'pardot_url' => 'www.example.com',
        'pardot_fields_mapping' => '',
      ],
    ];
    $webform_handler->getConfiguration()
      ->willReturn($config);

    return $webform_handler->reveal();
  }

  /**
   * Mocks a guzzle http response.
   */
  public function mockResponse($error = FALSE) {
    $response = $this->prophesize(ResponseInterface::class);

    $response->getBody()->willReturn($this->mockFnStream());

    if ($error) {
      $response->getStatusCode()->willReturn('200');
    }
    else {
      $response->getStatusCode()->willReturn('503');
    }

    return $response->reveal();
  }

  /**
   * Mocks fn steam.
   */
  public function mockFnStream() {
    $fn_stream = $this->prophesize(FnStream::class);

    $fn_stream->getContents()->willReturn('Succesfull');

    return $fn_stream->reveal();
  }

}
