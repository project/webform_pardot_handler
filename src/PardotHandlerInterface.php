<?php

namespace Drupal\webform_pardot_handler;

/**
 * Provides an interface for posting webform data to Pardot.
 */
interface PardotHandlerInterface {

  /**
   * Pardot submission queued status.
   */
  const PARDOT_QUEUED = 'queued';

  /**
   * Pardot submission processed status.
   */
  const PARDOT_PROCESSED = 'processed';

  /**
   * Pardot submission error status.
   */
  const PARDOT_ERROR = 'error';

  /**
   * Post webform data to Pardot url.
   *
   * @param string $pardot_submission_id
   *   The id of the pardot submission to be posted.
   */
  public function submitDataToPardot($pardot_submission_id);

}
