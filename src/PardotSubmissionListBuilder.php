<?php

namespace Drupal\webform_pardot_handler;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines the list builder for pardot submissions.
 */
class PardotSubmissionListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'id' => [
        'data' => $this->t('Pardot Submission ID'),
      ],
      'webform_submission' => [
        'data' => $this->t('Webforms Submission'),
      ],
      'status' => [
        'data' => $this->t('Status'),
      ],
      'log' => [
        'data' => $this->t('Log'),
      ],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [
      'id' => $entity->id(),
      'webform_submission' => NULL,
      'status' => $entity->status->value,
      'log' => $entity->log->value,
    ];

    $webform_submission = $entity->webform_submission->entity;
    if ($webform_submission) {
      $row['webform_submission'] = $webform_submission->toLink();
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    // We only want delete operation.
    $operations = parent::getDefaultOperations($entity);
    unset($operations['edit']);

    return $operations;
  }

}
