<?php

namespace Drupal\webform_pardot_handler\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a pardot_submission entity.
 *
 * @ingroup webform_pardot_handler
 */
class PardotSubmissionDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t(
      'Are you sure you want to delete entity %name?',
      ['%name' => $this->entity->id()]
    );
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the contact list.
   */
  public function getCancelUrl() {
    return new Url('entity.pardot_submission.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    $this->logger('webform_pardot_handler')->notice('deleted %id.',
      [
        '%id' => $this->entity->id(),
      ]
    );

    $form_state->setRedirect('entity.pardot_submission.collection');
  }

}
