<?php

namespace Drupal\webform_pardot_handler\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines methods for PardotSubmission entities.
 *
 * @package Drupal\webform_pardot_handler\Entity
 */
interface PardotSubmissionInterface extends ContentEntityInterface {

  /**
   * Add log to a pardot submission entity.
   *
   * @param string $log
   *   The log message.
   *
   * @return $this
   */
  public function addLog($log);

  /**
   * Set status of a pardot submission entity.
   *
   * @param string $status
   *   The status string.
   *
   * @return $this
   */
  public function setStatus($status);

  /**
   * Provides the status of a pardot submission entity.
   *
   * @return string
   *   The pardot submission status.
   */
  public function getStatus();

  /**
   * Provides the webform associated with a pardot submission.
   *
   * @return Drupal\webform\webformSubmissionInterface
   *   The webform submission entity.
   */
  public function getWebformSubmission();

}
