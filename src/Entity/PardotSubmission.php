<?php

namespace Drupal\webform_pardot_handler\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Pardot Submission entity.
 *
 * @ContentEntityType(
 *   id = "pardot_submission",
 *   label = @Translation("Pardot Submission"),
 *    handlers = {
 *      "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\webform_pardot_handler\PardotSubmissionListBuilder",
 *      "form" = {
 *       "delete" = "Drupal\webform_pardot_handler\Form\PardotSubmissionDeleteForm",
 *     },
 *     "access" = "Drupal\webform_pardot_handler\PardotSubmissionAccessControlHandler",
 *   },
 *   base_table = "pardot_submission",
 *   data_table = "pardot_submission_data",
 *   admin_permission = "administer site settings",
 *   fieldable = TRUE,
 *   translatable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "delete-form" = "/admin/webform_pardot_handler/{pardot_submission}/delete",
 *     "collection" = "/admin/structure/webform/pardot_submissions"
 *   },
 * )
 */
class PardotSubmission extends ContentEntityBase implements PardotSubmissionInterface {

  /**
   * {@inheritdoc}
   */
  public function addLog($log) {
    $this->get('log')->appendItem($log);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getWebformSubmission() {
    return $this->webform_submission->entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type
  ) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Unique ID field of pardot submission.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Pardot Submission entity.'))
      ->setReadOnly(TRUE);

    // The webform submission entity associated with the pardot submission.
    $fields['webform_submission'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Webform Submission'))
      ->setDescription(t('The webform submission ID.'))
      ->setSetting('target_type', 'webform_submission')
      ->setSetting('handler', 'default')
      ->setSetting(
        'handler_settings',
        ['target_bundles' => ['webform' => 'webform']]
      );

    // The pardot submission status. Default value will be queued.
    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The pardot submission status.'))
      ->setSettings([
        'default_value' => 'queued',
        'max_length' => 255,
      ]);

    $fields['log'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Log'))
      ->setDescription(t('Log messages.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSettings([
        'default_value' => NULL,
        'max_length' => 500,
      ]);

    // The changed field type automatically updates the timestamp every time the
    // entity is saved.
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(
        t('The time that the pardot submission was last edited.')
      );

    return $fields;
  }

}
