<?php

namespace Drupal\webform_pardot_handler;

use Drupal\webform_pardot_handler\Entity\PardotSubmissionInterface;
use Drupal\webform\Entity\WebformSubmission;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Posts webform data to Pardot.
 */
class PardotHandler implements PardotHandlerInterface {

  use StringTranslationTrait;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a PardotHandler object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Http client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(
    ClientInterface $http_client,
    EntityTypeManagerInterface $entity_type_manager
    ) {
    $this->httpClient = $http_client;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function submitDataToPardot($pardot_submission_id) {
    $pardot_submission = $this->entityTypeManager
      ->getStorage('pardot_submission')
      ->load($pardot_submission_id);

    // Return FALSE if we cannot find a pardot submission.
    if (!$pardot_submission) {
      return FALSE;
    }

    // Fetch the webform submission associated with the pardot sentity.
    $webform_submission = $pardot_submission->getWebformSubmission();

    // Return FALSE if we cannot find a pardot submission.
    if (!$webform_submission) {
      return FALSE;
    }

    $url = $this->getPardotUrl($webform_submission);

    // Return FALSE if a pardot url is not set.
    if (!$url) {
      return FALSE;
    }

    $data = $webform_submission->getData();
    $mapping = $this->getPardotFieldsMapping($webform_submission);
    $data = $this->mapData($mapping, $data);

    // Execute remote post.
    $response = $this->remotePost($url, $data);

    // Process the response.
    return $this->processResponse($response, $pardot_submission);
  }

  /**
   * Maps data according to a mapping array.
   *
   * @param string $mapping
   *   A list of mapping rules, one per line.
   * @param array $data
   *   The data to be mapped.
   *
   * @return array
   *   The mapped data.
   */
  private function mapData($mapping, array $data) {
    if (!empty($mapping)) {
      // Parse mapping.
      $mapping_info = [];
      $ret = explode(PHP_EOL, $mapping);
      foreach ($ret as $r) {
        $line = explode('|', trim($r));
        if (count($line) == 2) {
          $mapping_info[$line[0]] = $line[1];
        }
      }

      // Re map keys with new mapping and leave the rest as they are.
      $mapped_data = [];
      foreach ($data as $key => $value) {
        if (!empty($mapping_info[$key])) {
          $mapped_data[$mapping_info[$key]] = $value;
        }
        else {
          $mapped_data[$key] = $value;
        }
      }

      // Are there any second or further level mapping?
      foreach ($mapping_info as $key => $mapped_key) {
        $flat_key = $key;

        // Array notation to dot notation.
        if (strpos($key, '[') !== FALSE) {
          $flat_key = str_replace(['[', ']'], ['.', ''], $key);
        }

        // Parse dot-notated key.
        if (strpos($flat_key, '.') !== FALSE) {
          $flat_key_array = explode('.', $flat_key);
          $mapped_data = $this->combinedKeyAdd($mapped_data, $flat_key_array, $mapped_key);
        }
      }

      $data = $mapped_data;
    }

    return $data;
  }

  /**
   * Adds a combined key within an array.
   *
   * @param array $data
   *   Data array to add key to.
   * @param array $flat_key_array
   *   Multiarray key in "key.key1.key2" format.
   * @param string $mapped_key
   *   Name of the key to map into the array.
   *
   * @return array
   *   Array containing the combined key.
   *
   * @see https://stackoverflow.com/questions/7003559/use-strings-to-access-potentially-large-multidimensional-arrays
   */
  private function combinedKeyAdd(array $data, array $flat_key_array, $mapped_key) {
    $value = $data;
    foreach ($flat_key_array as $key) {
      // Checkboxes are stored as values.
      if (is_array($value) && in_array($key, $value)) {
        $value = $key;
      }
      else {
        $value = !empty($value[$key]) ? $value[$key] : $value;
      }
    }
    $data[$mapped_key] = $value;

    return $data;
  }

  /**
   * Posts data to a remote URL.
   *
   * @param string $url
   *   The URL to which the data should be posted.
   * @param array $data
   *   The data array to be posted to the url.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response object.
   */
  private function remotePost($url, array $data) {
    // Post data to the provided url.
    $response = $this->httpClient->post($url, [
      'verify' => TRUE,
      'form_params' => $data,
      'headers' => [
        'Content-type' => 'application/x-www-form-urlencoded',
      ],
    ]);

    return $response;
  }

  /**
   * Provides the pardot URL configured in webform handler.
   *
   * @param \Drupal\webform\Entity\WebformSubmission $webform_submission
   *   The webform submission.
   *
   * @return string
   *   The pardot URL string.
   */
  private function getPardotUrl(
    WebformSubmission $webform_submission
  ) {
    return $webform_submission->getWebform()
      ->getHandler('submit_data_to_pardot')
      ->getConfiguration()['settings']['pardot_url'];
  }

  /**
   * Provides the pardot field mapping configured in webform handler.
   *
   * @param \Drupal\webform\Entity\WebformSubmission $webform_submission
   *   The webform submission.
   *
   * @return string
   *   The pardot field mapping string.
   */
  private function getPardotFieldsMapping(
    WebformSubmission $webform_submission
  ) {
    return $webform_submission->getWebform()
      ->getHandler('submit_data_to_pardot')
      ->getConfiguration()['settings']['pardot_fields_mapping'];
  }

  /**
   * Process the Pardot response data and updates the pardot submission entity.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   The response returned by the remote server.
   * @param \Drupal\webform_pardot_handler\Entity\PardotSubmissionInterface $pardot_submission
   *   The pardot submission entity.
   *
   * @return \Drupal\webform_pardot_handler\Entity\PardotSubmissionInterface
   *   Updated pardot submission entity.
   */
  private function processResponse(
    ResponseInterface $response,
    PardotSubmissionInterface $pardot_submission
    ) {
    $status_code = $response->getStatusCode();
    $log = $response->getBody()->getContents();
    $status = self::PARDOT_PROCESSED;

    // The pardot response would include text `field is required` if a form
    // validation error happens at Pardot.
    if (strpos($log, 'field is required') !== FALSE) {
      $log = 'Could not post data to pardot, If a field is required on your
      pardot form handler, make the field required on the webform';
      $status = self::PARDOT_ERROR;
    }

    // Log error message depending on the status code.
    switch ($status_code) {
      // 5xx status code would mean an error at Pardot end.
      case $status_code < 510 && $status_code >= 500:
        $log = 'Internal Server Error at Pardot';
        $status = self::PARDOT_ERROR;
        break;

      // If status code is other than 500 or 200 log a generic error message
      // with the status code.
      case $status_code < 200 || $status_code >= 300:
        $log = 'An unexpected error occured while submitting data to Pardot';
        $status = self::PARDOT_ERROR;
        break;
    }

    // Append status code along with the log message.
    $log = $status_code . ': ' . $log;

    // Make sure that the log only contains 500 characters.
    $log = substr($log, 0, 500);

    $pardot_submission->addLog($log);
    $pardot_submission->setStatus($status);
    $pardot_submission->save();

    return $pardot_submission;
  }

}
