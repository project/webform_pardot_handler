<?php

namespace Drupal\webform_pardot_handler\Plugin\QueueWorker;

use Drupal\webform_pardot_handler\PardotHandlerInterface;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Posts webform submissions to Pardot.
 *
 * @QueueWorker(
 *   id = "pardot_submission_queue",
 *   title = @Translation("Pardot Submission queue"),
 *   cron = {"time" = 60}
 * )
 */
class PardotSubmissionQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Pardot integration handler.
   *
   * @var Drupal\webform_pardot_handler\PardotHandlerInterface
   */
  protected $pardotHandler;

  /**
   * Constructs a new Pardot submission queue object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\webform_pardot_handler\PardotHandlerInterface $pardot_handler
   *   The pardot handler.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    PardotHandlerInterface $pardot_handler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->pardotHandler = $pardot_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('webform_pardot_handler.pardot_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Post data to pardot.
    $this->pardotHandler->submitDataToPardot(
      $data['pardot_submission_id']
    );
  }

}
