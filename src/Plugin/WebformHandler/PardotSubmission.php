<?php

namespace Drupal\webform_pardot_handler\Plugin\WebformHandler;

use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "pardot_submission",
 *   label = @Translation("Pardot handler"),
 *   category = @Translation("Form Handler"),
 *   description = @Translation("Submit data to Pardot in background with cron"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class PardotSubmission extends WebformHandlerBase {

  /**
   * Pardot integration handler.
   *
   * @var \Drupal\webform_pardot_handler\PardotHandlerInterface
   */
  protected $pardotHandler;

  /**
   * Queue Factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->configFactory = $container->get('config.factory');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->conditionsValidator = $container->get('webform_submission.conditions_validator');
    $instance->pardotHandler = $container->get('webform_pardot_handler.pardot_handler');
    $instance->queueFactory = $container->get('queue');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form['pardot_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Pardot post url'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['pardot_url'],
    ];

    $form['pardot_fields_mapping'] = [
      '#type' => 'textarea',
      '#required' => FALSE,
      '#title' => $this->t('Mapping of webform fields to Pardot fields'),
      '#description' => $this->t(
        'If Pardot uses different keys, you can map them here.\r\n
        Any key not included will be left as it is found on the webform.\r\n
        A list of "webform_key|pardot_key" is expected for each line.\r\n
        Values can be extracted from complex webform elements by writing the\r\n
        path to a value, separated by ".". For example, "person.name|name"'
      ),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['pardot_fields_mapping'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValues();
    $values['pardot_url'] = trim($values['pardot_url']);
    $form_state->setValues($values);
    if (!filter_var($values['pardot_url'], FILTER_VALIDATE_URL)) {
      $form_state->setErrorByName('pardot_url', $this->t('Pardot post url must be a valid URL.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'pardot_url' => NULL,
      'pardot_fields_mapping' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(
    WebformSubmissionInterface $webform_submission,
    $update = TRUE
  ) {
    // Create pardot submission entity with status queued.
    $pardot_submission = $this->createPardotSubmission($webform_submission);

    // Add the pardot submission in a queue to post data pardot.
    $values['pardot_submission_id'] = $pardot_submission->id();
    $queue = $this->queueFactory->get('pardot_submission_queue');
    $queue->createItem($values);
  }

  /**
   * Creates a pardot submission entity.
   *
   * A pardot submission entity is created in status queued.
   *
   * @param Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission.
   *
   * @return Drupal\webform_pardot_handler\Entity\PardotSubmission
   *   The pardot submission entity.
   */
  private function createPardotSubmission(
    WebformSubmissionInterface $webform_submission
  ) {
    $pardot_submission = $this->entityTypeManager
      ->getStorage('pardot_submission')
      ->create([
        'webform_submission' => $webform_submission,
        'status' => $this->pardotHandler::PARDOT_QUEUED,
      ]);
    $pardot_submission->save();

    return $pardot_submission;
  }

}
